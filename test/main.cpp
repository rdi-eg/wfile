#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "rdi_wfile.hpp"

#include <rdi_stl_utils.hpp>

using namespace std;
using namespace RDI;

static string tests_path = string(CODE_LOCATION) + "tests/";

static vector<string> expected_directory_content =
{
    "long_names",
	"test.sh",
    "test1",
    "test2",
    "test3"
};

static map<string , string> expected2 =
{
    {"NNET3MODEL" , "final.mdl"},
    {"DICTIONARY" , "words.txt"},
    {"GRAPH" , "HCLG.fst"},
    {"WORD_INS_PENALTY" , "5.0"},
    {"ACOUSTIC_SCALE" , "0.1"},
    {"LATTICE_SCALE" , "6.0"},
    {"BEAM" , "15"},
    {"LATTICE_BEAM" , "8.0"},
    {"MAX_ACTIVE" , "7000"},
    {"USING_GPU" , "no"}

};

static vector<vector<float>> expected3 =
{
    {1.5f , 5.2f , 6.56f},
    {12.5f , 53.2f , 26.56f},
    {1.2315f , 52.2f , 36.56f}
};

static vector<vector<float>>
convert_str_to_float(vector<string>&& input){

    vector<float> temp;
    vector<vector<float>> res;

    for(const auto& item :input){
        vector<string> vs = split(item,' ');
        for(const auto& str : vs){
            temp.push_back(stof(str));
        }
        res.push_back(temp);
        temp.clear();
    }
    return res;
}

TEST_CASE("write and read")
{

    string content = "sdfsdf";
    wstring content1 = L"يsdfsdf";
    RDI::write_file("string_file.txt",content);
    RDI::write_wfile("wstring_file.txt", content1);

    string read_content = RDI::read_file("string_file.txt");
    wstring read_content1 = RDI::read_wfile("wstring_file.txt");

    CHECK(content == read_content);
    CHECK(content1 == read_content1);

    RDI::delete_file("string_file.txt");
    RDI::delete_file("wstring_file.txt");
}

TEST_CASE("Find Files that")
{

    auto files
        = find_files_that(string(CODE_LOCATION),
                          [](const RDI::fs::path& entry) {
                              return entry.extension() == ".cpp";
                          });
    CHECK(extract_filename(files[0])=="main.cpp");
}

TEST_CASE( "Create Directory" )
{

    CHECK( create_directory( tests_path + "testDirectory/") );
    CHECK( create_directory( tests_path + "../a/b/c", true ));
    delete_directory(tests_path + "../a/b/c");
    delete_directory(tests_path + "../a/b");
    delete_directory(tests_path + "../a");
}

TEST_CASE( "Delete Directory" )
{
    CHECK( delete_directory( tests_path + "testDirectory" ) );
}

TEST_CASE( "Create File" )
{
    CHECK( write_file( tests_path + "testFile", "bleh") );
}

TEST_CASE( "Delete File" )
{
   CHECK(delete_file( tests_path + "testFile" ) );
   //CHECK_THROWS(delete_file( tests_path + "testFile" ) );
}

TEST_CASE( "Get Directory Content" )
{
    CHECK( get_directory_content(tests_path) == expected_directory_content );
}

TEST_CASE( "Get Absolute Path" )
{
#ifdef _MSC_VER
    CHECK(get_absolute_path(tests_path)[1] == ':'); // as in C: or D:
    CHECK(get_absolute_path("C:\\var") == "C:\\var");
#else
    CHECK(get_absolute_path(tests_path)[0] == '/');
    CHECK(get_absolute_path("/var") == "/var");
#endif

}

TEST_CASE("get_current_directory")
{
    cout << get_current_directory() << endl;
}

TEST_CASE( "Dump Matrix")
{
    REQUIRE( dump_matrix((tests_path + "test3"), expected3) == true);
    REQUIRE(convert_str_to_float( read_file_lines(tests_path + "test3") ) == expected3);
}

TEST_CASE( "Check If Given Path is a Directory" )
{
    CHECK( is_directory(tests_path) );
    CHECK_FALSE( is_directory(tests_path + "test1") );

#ifdef _MSC_VER
    CHECK(is_directory("C:\\Windows\\"));
#else
    CHECK(is_directory("/usr/bin/"));
#endif
}

#ifdef __linux__
TEST_CASE( "check if given path has execution permission" )
{
	CHECK( is_executable("tests_path/test.sh") );
}
#endif // __linux__

TEST_CASE("check if file exists")
{
    REQUIRE(file_exists(tests_path + "test1"));
    REQUIRE(! file_exists(tests_path + "test4"));
}

TEST_CASE( "Get File Name from Path" )
{
    CHECK		( extract_filename("/bin/bash") == "bash" );
    CHECK		( extract_filename("/lib64/ld-linux-x86-64.so.2") == "ld-linux-x86-64.so.2" );
    CHECK		( extract_filename_without_extension("/bin/bash") == "bash" );
    CHECK		( extract_filename_without_extension("/lib64/ld-linux-x86-64.so.2") == "ld-linux-x86-64.so" );
/*
#ifdef _MSC_VER
    CHECK_THROWS(extract_filename_without_extension("C:\\users"));
    CHECK_THROWS(extract_filename_without_extension("C:\\users\\"));
#else
    CHECK_THROWS(extract_filename_without_extension("/usr/local/lib"));
    CHECK_THROWS(extract_filename_without_extension("/usr/local/lib/"));
#endif
*/
}

TEST_CASE("Get Path from File Name")
{
#ifdef _MSC_VER
    CHECK(extract_path_from_filename("bin\\bash") == "bin/");
    CHECK(extract_path_from_filename("lib64\\ld-linux-x86-64.so.2") == "lib64/");
    CHECK(extract_path_from_filename("usr\\local\\lib/") == "usr\\local\\lib/");
#else
    CHECK( extract_path_from_filename("/bin/bash") == "/bin/" );
    CHECK( extract_path_from_filename("/lib64/ld-linux-x86-64.so.2") == "/lib64/" );
    CHECK( extract_path_from_filename("/usr/local/lib/") == "/usr/local/lib/" );
#endif
}

TEST_CASE( "extract extention from path" )
{
    string extension;
    extension = extract_extention_from_path("/000000000000_00002.wav");
    CHECK( extension == ".wav");

    extension = extract_extention_from_path("output/all_lines");
    CHECK( extension.empty() );
}

TEST_CASE("R/W binary files")
{
    string quote = "إن مشكلة العالم هى أن الأغبياء و المتعصبين دائما\n\r "
            "واثقين من أنفسهم فى حين أن العقلاء تملئهم الشكوك فى أنفسهم"
               "\n اقباس من bertrand russell  ";


    // testing the bytes are written and read correctly.
    auto error = RDI::write_binary_file(L"./مقولة عن فيلسوف.txt", vector<uint8_t>(quote.cbegin(),quote.cend()));
    assert( error == 0);

    std::vector<char> bytes = read_binary_file(L"./مقولة عن فيلسوف.txt");
    CHECK( string(bytes.cbegin(), bytes.cend()) == quote );

    // test the R/W functions and runtime errors correctly.
    vector<char> love = {'l', 'o', 'v', 'e'};
    int error2 = write_binary_file("/home/life.txt", love);
    auto [ bytes1, error3] = read_binary_file("/home/life.txt");
    CHECK(error2 == error3);

    delete_file("مقولة عن فيلسوف.txt");
}

#ifdef _WIN32
TEST_CASE("long filename")
{
    wstring file_path = wstring(WCODE_LOCATION)+ L"tests/long_names/dlkafgnlfgnlkrgerkgm-324923trtf42tjt40hrgi3gh3348t8ugj35g8ty3gh8dfsge832نيبتلخيبلىثمقنصفا408قبتهببى2ف2غا776454ككطط2ق54@$!#$!#-!@$!(-$-bvfjksgbkfdsnvgjkrengkjgnfjknf;kagnrgjnfjkbfakjgbjgakfjvfkjnffndg.txt";
    auto content = read_binary_file(file_path);
    int error = write_binary_file(file_path, {'f', 'r', 'e', 'e', 'd', 'o', 'm'});
}
#endif

TEST_CASE("test deprecated function")
{
	// should get a warning message here that says:
	// 'extract_filename_without_extension' is deprecated: Consider upgrading to
	// extract_filename_without_extension_v2
	extract_filename_without_extension("/lib64/ld-linux-x86-64.so.2");
}

