# wfile
A little C++ lib that helps you do file operations with string and wstrings.

* uses c++ filesystem (u need to link with  -lstdc++fs)

We faced some issues when dealing with non-latin characters, so we created this library to help us with file I/O operations.

# Usage

Just include `rdi_wfile.hpp` and call any of the following functions.
```c++
namespace RDI
{

using fs_path = std::experimental::filesystem::path;

std::wstring read_wfile(const std::string& filename);

std::vector<std::wstring> read_wfile_lines(const std::string& filename);

std::string read_file(const std::string& filename);

std::vector<std::string> read_file_lines(const std::string& filename);

bool write_wfile(const std::string& filename, const std::wstring& fileContent);

bool append_to_wfile(const std::string& filename, const std::wstring& content);

bool write_wfile_lines(const std::string& filename,
					   const std::vector<std::wstring>& linesToWrite);

bool write_file(const std::string& filename, const std::string& fileContent);

bool append_to_file(const std::string& filename, const std::string& content);

bool write_file_lines(const std::string& filename,
					  const std::vector<std::string>& linesToWrite);

///  return absolute pathes to files that mathces the predicate
///	 auto files = find_files_that(path); will return all files in that path
///  auto files = find_files_that(asdf,[](const fs::path &p)
///								 {return p.extension() == ".cpp";});
///  will return all files in that path that ends with .cpp
std::vector<std::string>
find_files_that(const fs_path& dir,
				std::function<bool(const fs_path&)> filter
				= [](const fs_path&) { return true; },
				const bool recurse = false, const bool follow_symlinks = false);

bool delete_file(std::string_view path);

std::string get_absolute_path(std::string_view);

/// returns the location of the binary executable ex: "/home/rdi/bin"
std::string get_current_directory();

std::vector<std::string> get_directory_content(std::string_view path);

bool create_directory(std::string_view path, bool nested = false);

bool delete_directory(std::string_view path);

bool dump_matrix(const std::string& file_name,
				 std::vector<std::vector<float>>& input_matrix);

bool file_exists(std::string_view filename);

bool is_directory(std::string_view path);

std::string extract_path_from_filename(std::string_view filename);

std::string extract_filename(const std::string& path);

std::string extract_filename_without_extension(const std::string& path);

std::string extract_extention_from_path(std::string_view filename);

} // namespace RDI
```